# X12 Parser
Package to parse ANSI X12 EDI files

## Features
 * Parse by Loop, Segment and Element
 * Supports 270/271, 835/837

    
### How do I use this package?
1. Start Session by selecting the EDI file and X12 version
2. Parse specific data elements, or capture all Loops
3. Translate all loops to JSON
 

